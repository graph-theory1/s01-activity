import pandas as pd

class Graph1:
	def __init__(self, directed=True):
		
		self.m_directed=directed
		self.m_list_of_edges = []
		
	def add_edge(self, node1, node2, weight=1):
		self.m_list_of_edges.append([node1,node2,weight]) 

		if not self.m_directed:
			self.m_list_of_edges.append([node1,node2,weight]) 

	def print_adj_matrix(self):
		num_of_edges = len(self.m_list_of_edges)
		for i in range(num_of_edges):
			print("Edge", i+1, ": ", self.m_list_of_edges[i])


graph1 =Graph1()

graph1.add_edge("CA", "MA", 50)
graph1.add_edge("FL", "CA", 25)
graph1.add_edge("NY", "WA", 70)
graph1.add_edge("NY", "NJ", 41)
graph1.add_edge("WA", "FL", 67)
graph1.add_edge("NJ", "WA", 34)
graph1.add_edge("NJ", "NY", 41)

print("distances among 5 states of US")
graph1.print_adj_matrix()

class Graph2:
	def __init__(self, num_of_nodes, n, directed=True):
		self.m_num_of_nodes=num_of_nodes
		self.m_directed=directed
		self.m_adj_matrix = [[0 for column in range(num_of_nodes)] for row in range(num_of_nodes)]
		self.adj_df = pd.DataFrame(self.m_adj_matrix, columns=[c for c in n], index=[r for r in n])

	def add_edge(self, node1, node2, weight=1):
		self.adj_df[node2][node1] = weight

		if not self.m_directed:
			self.adj_df[node2][node1] = weight

	def print_adj_matrix(self):
		print(self.adj_df)

n = [0,1,2,3,4]
graph2 =Graph2(5, n, directed=False)

graph2.add_edge(1, 0, 8)
graph2.add_edge(0, 1, 8)
graph2.add_edge(0, 2, 10)
graph2.add_edge(1, 3, 4)
graph2.add_edge(1, 4, 11)
graph2.add_edge(2, 0, 10)
graph2.add_edge(2, 4, 2)
graph2.add_edge(3, 1, 4)
graph2.add_edge(3, 4, 12)
graph2.add_edge(4, 1, 11)
graph2.add_edge(4, 2, 2)
graph2.add_edge(4, 3, 12)
graph2.add_edge(4, 4, 3)

print("adjacency matrix")
graph2.print_adj_matrix()

class Graph3:
	def __init__(self, nodes, directed=True):
		
		self.m_directed=directed
		self.m_adj_list = {node: list() for node in nodes}
		

	def add_edge(self, node1, node2, weight=1):
		self.m_adj_list[node1].append((node2,weight))

		if not self.m_directed:
			if node1 == node2:
				pass
			else:
				self.m_adj_list[node2].append((node1, weight))

	def print_adj_list(self):
		for key in self.m_adj_list.keys():
			print("node", key, ": ", self.m_adj_list[key])

n2 = [1,2,3,4,5]
graph3 =Graph3(n2, directed=True)

graph3.add_edge(2, 1, 1)
graph3.add_edge(5, 5, 1)
graph3.add_edge(5, 1, 1)
graph3.add_edge(5, 3, 1)
graph3.add_edge(4, 2, 1)
graph3.add_edge(4, 3, 1)

print("directed adjacency list")
graph3.print_adj_list()



graph4 =Graph3(n2, directed=False)

graph4.add_edge(2, 1, 1)
graph4.add_edge(5, 5, 1)
graph4.add_edge(5, 1, 1)
graph4.add_edge(5, 3, 1)
graph4.add_edge(4, 2, 1)
graph4.add_edge(4, 3, 1)

print("undirected adjacency list")
graph4.print_adj_list()